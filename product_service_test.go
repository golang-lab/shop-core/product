package main

import (
	"context"
	"testing"

	pb "gitlab.com/golang-lab/shop-proto/proto/product"
)

func TestProductGet(t *testing.T) {
	service := &Service{}
	res, err := service.Get(context.TODO(), &pb.ProductRequest{
		Seq:      "",
		BrandId:  "",
		OriginId: "",
	})
	if err != nil {
		t.Fatal(err)
	}

	if res.Total <= 0 || int(res.Total) != len(res.ProductList) {
		t.Errorf("return unexpected total:%v", res.Total)
	}

}
