package main

import (
	"context"

	pb "gitlab.com/golang-lab/shop-proto/proto/product"
)

type Service struct{}

func (*Service) Get(ctx context.Context, req *pb.ProductRequest) (*pb.ProductResponse, error) {

	res := &pb.ProductResponse{
		Total: 3,
		ProductList: []*pb.ProductResponse_ProductDetail{
			{Seq: "1", Name: "夏季騎行機車安全帽", BrandId: "ZEUS", Price: 1200, Quantity: 15, OriginId: "Taiwan"},
			{Seq: "2", Name: "全罩式霧黑安全帽", BrandId: "Yamaha", Price: 2580, Quantity: 3, OriginId: "Japan"},
			{Seq: "3", Name: "全罩式雙面罩安全帽", BrandId: "Yamaha", Price: 2980, Quantity: 35, OriginId: "Japan"},
		},
	}
	return res, nil
}
