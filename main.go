package main

import (
	"log"
	"net"

	pb "gitlab.com/golang-lab/shop-proto/proto/product"
	"google.golang.org/grpc"
)

func main() {

	listener, err := net.Listen("tcp", "localhost:50051")
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}

	server := grpc.NewServer()

	pb.RegisterProductServiceServer(server, &Service{})

	if err = server.Serve(listener); err != nil {
		log.Fatalf("failed to serve %v", err)
	}
}
